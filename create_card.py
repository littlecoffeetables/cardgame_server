import json

gem_alias = {"n":"neutral","g":"green","b":"blue","r":"red"}

def do(cardid, def_file):
	obj = {}
	scripts = []
	for prop in ("name", "type", "subtypes", "stats", "desc", "unit_event_listeners", "special_scripts", "flavor"):
		if prop == "stats":
			stats = {}
			for stat in ("atk", "speed", "hp", "cost"):
				val = input("%s: " % stat)
				if val != "":
					stats[stat] = int(val)
			colors = input("color letter code: ")
			if colors != "":
				gems = {}
				for letter in colors:
					name = gem_alias.get(letter)
					if name is None:
						print("Skipping unrecognised letter", letter)
						continue
					val = input("%s gems: " % name)
					gems[name] = int(val)
				stats["gems"] = gems
			obj["stats"] = stats
		else:
			val = input("%s: " % prop)
			if val != "":
				if prop in ("subtypes",):
					val = list(val.split(" "))
				elif prop in ("unit_event_listeners", "special_scripts"):
					val = list(val.split(" "))
					scripts += val
				obj[prop] = val

	final_obj = {"id":cardid, "versions":[obj]}
	with open(def_file, "w") as f:
		json.dump(final_obj, f, indent="\t")
		print("Done.")
	if len(scripts) > 0 or input("Make py file? ") == "yes":
		with open("game_server/card_scripts/%s.py" % cardid, "w") as f:
			f.write("\n")
			for script in scripts:
				f.write("def %s(this_unit, const_match, event):\n\tpass\n" % script)


def main():
	cardid = input("id: ")
	def_file = "data/cards/%s.json" % cardid
	try:
		f = open(def_file)
		f.close()
		print("Card definition file already exists.")
		if input("Overwrite? ") == "yes":
			do(cardid, def_file)
	except FileNotFoundError:
		do(cardid, def_file)

main()
