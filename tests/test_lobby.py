import asyncio

import unittest

import test_utils
test_utils.modify_py_path()

from game_server.lobby import *
from game_server.user import *

class FakeUser(User):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fake_sent = []
		self.quit_called = 0

	def quit(self):
		super().quit()
		self.quit_called += 1

	async def recv(self):
		if self._quit_fut:
			await self._quit_fut
	
	async def send_raw(self, data):
		self.fake_sent.append(data)
		if self._quit_fut and not self._quit_fut.done():
			self._quit_fut.set_result(None)

	@property
	def quit_fut(self):
		return self._quit_fut


class LobbyTestCase(unittest.TestCase):
	def test_add_and_rm_user(self):
		lobby = Lobby(print_broadcasts=False)
		user = FakeUser(None, None, "joe123")
		
		lobby.add_user(user)
		
		self.assertTrue(user.lobby is lobby)
		self.assertTrue(user in lobby.users.values())

		lobby.rm_user(user)

		self.assertTrue(user.lobby is None)
		self.assertFalse(user in lobby.users.values())

	def test_broadcast_message(self):
		loop = asyncio.get_event_loop()
		lobby = Lobby(print_broadcasts=False)
		user1 = FakeUser(None, asyncio.Future(), "joe123")
		user2 = FakeUser(None, asyncio.Future(), "Bobby")
		lobby.add_user(user1)
		loop.run_until_complete(user1.quit_fut)
		lobby.add_user(user2)
		loop.run_until_complete(user2.quit_fut)

		loop.run_until_complete( lobby.broadcast_message("Hi") )

		server_str = "[Server]"
		self.assertEqual(user1.fake_sent, [
			make_message("joe123 joined",author=server_str),
			make_message("Bobby joined",author=server_str),
			make_message("Hi",author=server_str)
		])

		loop.run_until_complete( lobby.broadcast_message("Again") )

		self.assertEqual(user2.fake_sent, [
			make_message("Bobby joined",author=server_str),
			make_message("Hi",author=server_str),
			make_message("Again",author=server_str)
		])

	def test_match_queue(self):
		user = FakeUser(None, None, "johndoe")
		lobby = Lobby(print_broadcasts=False)
		lobby.add_user(user)
		self.assertFalse(user.id in lobby.standard_match_queue)

		lobby.add_user_to_queue(user, "standard")
		self.assertTrue(user.id in lobby.standard_match_queue)

		lobby.rm_user_from_queue(user)
		self.assertFalse(user.id in lobby.standard_match_queue)

	def test_shutdown(self):
		loop = asyncio.get_event_loop()
		user1 = FakeUser(None, asyncio.Future(), "joe")
		user2 = FakeUser(None, asyncio.Future(), "anAndroid")
		lobby = Lobby(print_broadcasts=False)
		lobby.add_user(user1)
		lobby.add_user(user2)
		loop.stop()
		loop.run_forever()
		self.assertTrue(user1.quit_called == 0)
		self.assertTrue(user2.quit_called == 0)
		loop.run_until_complete(lobby.shutdown())
		self.assertTrue(user1.quit_called == 1)
		self.assertTrue(user2.quit_called == 1)



if __name__ == "__main__":
	unittest.main()