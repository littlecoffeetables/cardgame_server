import sys
import os

def modify_py_path():
	path = os.path.join(os.path.dirname(os.path.dirname(__file__)), ".")
	if path not in sys.path:
		sys.path.insert(0, path)
