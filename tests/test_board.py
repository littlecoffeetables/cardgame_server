import json

import unittest

import test_utils
test_utils.modify_py_path()

from game_server.board import *


class BoardTestCase(unittest.TestCase):
	def test_init(self):
		board = Board(None)
		self.assertEqual(repr(board), "<Tile(HexCoords(x=0, y=0, z=0))>, <Tile(HexCoords(x=-1, y=1, z=0))>, <Tile(HexCoords(x=-2, y=2, z=0))>, <Tile(HexCoords(x=-3, y=3, z=0))>, <Tile(HexCoords(x=-4, y=4, z=0))>, <Tile(HexCoords(x=-4, y=3, z=1))>, <Tile(HexCoords(x=-4, y=2, z=2))>, <Tile(HexCoords(x=-4, y=1, z=3))>, <Tile(HexCoords(x=-3, y=0, z=3))>, <Tile(HexCoords(x=-2, y=-1, z=3))>, <Tile(HexCoords(x=-1, y=-2, z=3))>, <Tile(HexCoords(x=0, y=-3, z=3))>, <Tile(HexCoords(x=1, y=-4, z=3))>, <Tile(HexCoords(x=2, y=-4, z=2))>, <Tile(HexCoords(x=1, y=-3, z=2))>, <Tile(HexCoords(x=0, y=-2, z=2))>, <Tile(HexCoords(x=-1, y=-1, z=2))>, <Tile(HexCoords(x=-2, y=0, z=2))>, <Tile(HexCoords(x=-3, y=1, z=2))>, <Tile(HexCoords(x=-2, y=1, z=1))>, <Tile(HexCoords(x=-3, y=2, z=1))>, <Tile(HexCoords(x=-1, y=0, z=1))>, <Tile(HexCoords(x=0, y=-1, z=1))>, <Tile(HexCoords(x=1, y=-2, z=1))>, <Tile(HexCoords(x=2, y=-3, z=1))>, <Tile(HexCoords(x=3, y=-4, z=1))>, <Tile(HexCoords(x=4, y=-4, z=0))>, <Tile(HexCoords(x=3, y=-3, z=0))>, <Tile(HexCoords(x=2, y=-2, z=0))>, <Tile(HexCoords(x=1, y=-1, z=0))>, <Tile(HexCoords(x=2, y=-1, z=-1))>, <Tile(HexCoords(x=1, y=0, z=-1))>, <Tile(HexCoords(x=0, y=1, z=-1))>, <Tile(HexCoords(x=-1, y=2, z=-1))>, <Tile(HexCoords(x=-2, y=3, z=-1))>, <Tile(HexCoords(x=-3, y=4, z=-1))>, <Tile(HexCoords(x=-2, y=4, z=-2))>, <Tile(HexCoords(x=-1, y=3, z=-2))>, <Tile(HexCoords(x=0, y=2, z=-2))>, <Tile(HexCoords(x=1, y=1, z=-2))>, <Tile(HexCoords(x=2, y=0, z=-2))>, <Tile(HexCoords(x=3, y=-1, z=-2))>, <Tile(HexCoords(x=3, y=-2, z=-1))>, <Tile(HexCoords(x=4, y=-3, z=-1))>, <Tile(HexCoords(x=4, y=-2, z=-2))>, <Tile(HexCoords(x=4, y=-1, z=-3))>, <Tile(HexCoords(x=3, y=0, z=-3))>, <Tile(HexCoords(x=2, y=1, z=-3))>, <Tile(HexCoords(x=1, y=2, z=-3))>, <Tile(HexCoords(x=0, y=3, z=-3))>, <Tile(HexCoords(x=-1, y=4, z=-3))>")

	def test_adjacent_coords(self):
		board = Board(None)
		coords = HexCoords(-1,0,1)
		expected = [HexCoords(-1,1,0),HexCoords(0,0,0),HexCoords(-2,1,1),HexCoords(-2,0,2),HexCoords(-1,-1,2),HexCoords(0,-1,1)]
		for adj_coords in board.adjacent_coords(coords):
			try:
				expected.remove(adj_coords)
			except ValueError:
				self.fail("1:Got unexpected coords: %s" % adj_coords)
		self.assertTrue(len(expected) == 0)
		coords = HexCoords(4,-4,0)
		expected = [HexCoords(4,-3,-1),HexCoords(3,-3,0),HexCoords(3,-4,1)]
		for adj_coords in board.adjacent_coords(coords):
			try:
				expected.remove(adj_coords)
			except ValueError:
				self.fail("2:Got unexpected coords: %s" % adj_coords)
		self.assertTrue(len(expected) == 0)

	def test_adjacent_units(self):
		board = Board(None)
		coords = HexCoords(-1,0,1)
		for adj_unit in board.adjacent_units(coords):
			self.fail("Got a unit: %s" % adj_unit)





if __name__ == "__main__":
	unittest.main()