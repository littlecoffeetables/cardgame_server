import json

import unittest

import test_utils
test_utils.modify_py_path()

from game_server.user import *

class UserNameTestCase(unittest.TestCase):
	def test_valid_usernames(self):
		self.assertTrue(is_valid_username('abc'))
		self.assertTrue(is_valid_username('John_Doe'))
		self.assertTrue(is_valid_username('pretty_long_username'))
		self.assertTrue(is_valid_username('1a2b3c'))

	def test_invalid_usernames(self):
		self.assertFalse(is_valid_username('ab'))
		self.assertFalse(is_valid_username('waaaay_too_ridiculously_long_username'))
		self.assertFalse(is_valid_username("don't"))
		self.assertFalse(is_valid_username('no spaces'))

	def test_invalid_username_characters(self):
		self.assertTrue(is_valid_username(3 * '1'))
		for char in '!"#$%&/()=}]{[?^-+\\.,*~<>\'@':
			self.assertFalse(is_valid_username(3 * char))


class MessageTestCase(unittest.TestCase):
	def test_make_message(self):
		self.assertEqual( json.loads(make_message("derp")), {"type":"chat_message","message":"derp"} )


class UserTestCase(unittest.TestCase):
	def test_something_idk(self):
		pass





if __name__ == "__main__":
	unittest.main()