import game_server.main
import web_server.main

import multiprocessing

p = multiprocessing.Process(target=web_server.main.run)
p.start()
game_server.main.main()
p.terminate()
p.join()
