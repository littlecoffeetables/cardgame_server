from http.server import HTTPServer, BaseHTTPRequestHandler
import os

ROOT = "client"
HEADERS = {
    ".css": "text/css",
    ".json": "application/javascript",
    ".js": "application/javascript",
    ".ico": "image/x-icon",
}

class StaticServer(BaseHTTPRequestHandler):

    def do_GET(self):
        if self.path == '/':
            filename = ROOT + '/index.html'
        else:
            filename = ROOT + self.path

        if os.path.isfile(filename):
            self.send_response(200)
            for extension, header in HEADERS.items():
                if filename.endswith(extension):
                    self.send_header('Content-type', header)
                    break
            else:
                self.send_header('Content-type', 'text/html')
            self.end_headers()
            with open(filename, 'rb') as fh:
                self.wfile.write(fh.read())

        else:
            self.send_response(404)
            self.end_headers()


    def log_request(self, code='-', size='-'):
        pass


def run(server_class=HTTPServer, handler_class=StaticServer, port=8080):
    server_address = ('0.0.0.0', port)
    with server_class(server_address, handler_class) as httpd:
        print('Starting httpd on port {}'.format(port))
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            pass

if __name__ == "__main__":
    run()