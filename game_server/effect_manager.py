from . import const

from collections import defaultdict


class ConstEffectManager(const.BaseConst):
    ALLOWED = {
        "get_effects_by_owner",
        "get_effects_by_target"
    }

class EffectManager():
    def __init__(self, event_queue):
        self._effects_by_target = defaultdict(list)
        self._effects_by_owner = defaultdict(list)
        self._all_effects = list()
        self.event_queue = event_queue

    def add_effect(self, name, target_uuid, owner_uuid=None, modifiers=None, persistent=False, timer=None):
        effect = {"name": name}
        if target_uuid is not None:
            effect["target_uuid"] = target_uuid
            self._effects_by_target[target_uuid].append(effect)
        if owner_uuid is not None:
            effect["owner_uuid"] = owner_uuid
            self._effects_by_owner[owner_uuid].append(effect)
        if modifiers is not None:
            effect["modifiers"] = modifiers
        if persistent:
            effect["persistent"] = True
        if timer is not None:
            effect["timer"] = timer
        self._all_effects.append(effect)
        self.event_queue.put({
                "event": "effect_added",
                "effect": effect
            })

    def get_effects_by_owner(self, owner_uuid):
        return tuple(self._effects_by_owner[owner_uuid])

    def get_effects_by_target(self, target_uuid):
        return tuple(self._effects_by_target[target_uuid])

    def clear_effects_by_owner(self, owner_uuid, clear_persistent=False):
        for effect in self._effects_by_owner[owner_uuid]:
            if not clear_persistent and effect.get("persistent"):
                continue
            self.rm_effect(effect)

    def countdown(self):
        for effect in self._all_effects:
            time = effect.get("timer")
            if time is not None:
                effect["timer"] = time - 1
                if time <= 1:
                    self.rm_effect(effect)

    def rm_effect(self, effect):
        self._all_effects.remove(effect)
        if effect.get("owner_uuid") is not None:
            self._effects_by_owner[effect["owner_uuid"]].remove(effect)
        if effect.get("target_uuid") is not None:
            self._effects_by_target[effect["target_uuid"]].remove(effect)
        self.event_queue.put({
                "event": "effect_gone",
                "effect": effect
            })
