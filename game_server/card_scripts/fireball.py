
def spell(this_card, match, caster, target_tiles):
	assert(len(target_tiles) == 1)
	target_tile = target_tiles[0]
	assert(target_tile.is_occupied)
	target = target_tile.unit
	match.event_queue.put({"event":"unit_take_damage", "unit":target, "dmg_type":"fire", "amount":4})
