
def unit_moved(this_unit, const_match, event):
	const_match.runscript(this_unit, recalculate)

def unit_appeared(this_unit, const_match, event):
	const_match.runscript(this_unit, recalculate)

def unit_disappeared(this_unit, const_match, event):
	const_match.runscript(this_unit, recalculate)

def recalculate(this_unit, match, event):
	match.board.effect_manager.clear_effects_by_owner(this_unit.uuid)
	count = 0
	for other in match.board.adjacent_units(this_unit.coords):
		if "lizard" in other.subtypes:
			count += 1
	match.board.effect_manager.add_effect(
		name="packlizard_bonus",
		target_uuid=this_unit.uuid,
		owner_uuid=this_unit.uuid,
		modifiers={"atk": count}
		)
