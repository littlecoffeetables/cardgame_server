
def end_turn(this_unit, const_match, event):
	if event["from_player_id"] == this_unit.owner:
		if this_unit.get_effective_stat("speed") >= this_unit.get_max_stat("speed"):
			const_match.runscript(this_unit, give_gem)

def give_gem(this_unit, match, event):
	match.players[this_unit.owner].add_gems("blue", 1)

def can_attack(this_unit, const_board):
	return True
