from functools import partial

def unit_appeared(this_unit, const_match, event):
	if event["unit"].uuid == this_unit.uuid:
		const_match.event_queue.put({"event":"runscript","callback":partial(action, this_unit)})


def action(this_unit, match, event):
	if this_unit is not None:
		match.players[this_unit.owner].convert_gems("neutral", "red", limit=2)
