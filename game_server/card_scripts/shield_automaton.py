
def unit_moved(this_unit, const_match, event):
	if event["unit"] == this_unit:
		this_unit._special_unable_to_move = True

def really_change_turn(this_unit, const_match, event):
	this_unit._special_unable_to_move = False

def unit_appeared(this_unit, const_match, event):
	if event["unit"] == this_unit:
		this_unit._special_unable_to_move = False


def can_move(this_unit, board):
	return not this_unit._special_unable_to_move
