
def player_resources_changed(this_unit, const_match, event):
	const_match.runscript(this_unit, recalculate)

def recalculate(this_unit, match, event):
	match.board.effect_manager.clear_effects_by_owner(this_unit.uuid)
	count = match.players[this_unit.owner].get_gems("blue")
	match.board.effect_manager.add_effect(
		name="supersoldier_atk",
		target_uuid=this_unit.uuid,
		owner_uuid=this_unit.uuid,
		modifiers={"atk": count}
		)
