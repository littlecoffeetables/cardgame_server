
def unit_appeared(this_unit, const_match, event):
	count = 0
	opponent = 1 - this_unit.owner
	if event["unit"].uuid == this_unit.uuid:
		for unit in const_match.board.units:
			if unit.owner == opponent and unit.type == "creature":
				count += 1
	this_unit.modify_stat("max_hp", count)
	this_unit.modify_stat("hp", count)
