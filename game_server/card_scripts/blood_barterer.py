
def unit_take_damage(this_unit, const_match, event):
	if event["unit"].uuid == this_unit.uuid:
		const_match.runscript(this_unit, action)

def action(this_unit, match, event):
	match.players[this_unit.owner].convert_gems(from_='neutral', to='red', limit=1)
