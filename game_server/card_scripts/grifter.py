
def unit_appeared(this_unit, const_match, event):
	if event["unit"].uuid == this_unit.uuid:
		const_match.runscript(this_unit, hurt_commander)

def hurt_commander(this_unit, match, event):
	match.players[this_unit.owner].commander.take_damage(2)
