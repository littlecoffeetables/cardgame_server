
def sacced_card(this_unit, const_match, event):
	if event["player"] == this_unit.owner and event["sac_for"] == "blue":
		const_match.runscript(this_unit, give_card)

def give_card(this_unit, match, event):
	match.players[this_unit.owner].draw_cards(1)
