
def unit_attacked(this_unit, const_match, event):
	if event["unit"] == this_unit:
		this_unit.modify_stat("atk", 1)
		this_unit.modify_stat("speed", 1)
