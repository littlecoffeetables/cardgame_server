from . import deckbuilder
from . import cards

import asyncio
import websockets
import json
import re
import html
import logging


def is_valid_username(string):
	return re.match('^\\w{3,20}$', string) is not None


def make_message(message, author=None):
	obj = {
		"type": "chat_message",
		"message": html.escape(message)
	}
	if author and type(author) == str:
		obj["author"] = author
	return json.dumps(obj)


class User(object):
	""" a logged-in user
	ws: websocket
	name: username
	id: unique user id
	lobby: lobby user is in
	match: match instance the user is a player of
	_quit_fut: future that is to be set once the user disconnects
	"""
	enable_debug_command = True
	logger = logging.getLogger(__name__)
	#logger.setLevel(logging.INFO)

	def __init__(self, ws, quit_fut, name, userid=None):
		self._ws = ws
		self._quit_fut = quit_fut
		self._name = name
		if userid is None:
			self._id = name
		else:
			self._id = userid
		self._lobby = None
		self._match = None
		self._data_handlers = {}
		asyncio.ensure_future(self.recv())

	def __repr__(self):
		return "<User(%s, %s, name=%s, userid=%s) lobby=%s match=%s>" % (self.ws, self._quit_fut, self.name, self.id, self.lobby, self.match)

	@property
	def ws(self):
		return self._ws

	@property
	def name(self):
		return self._name

	@property
	def id(self):
		return self._id

	@property
	def lobby(self):
		return self._lobby

	@property
	def match(self):
		return self._match

	def quit(self):
		if not self._quit_fut.done():
			self._quit_fut.set_result(None)
			self.lobby.rm_user_from_queue(self)
			self.leave_match()
			asyncio.ensure_future(self.ws.close())
			if self.lobby is not None:
				self.lobby.rm_user(self)

	async def send_message(self, message, author=None):
		await self.send_raw(make_message(message, author=author))

	async def send_as_json(self, content):
		await self.send_raw(json.dumps(content))

	async def send_raw(self, data):
		try:
			User.logger.info("sending %s" % data)
			await self.ws.send(data)
		except websockets.exceptions.ConnectionClosed:
			self.quit()


	async def recv(self):
		try:
			while True:
				message = await self.ws.recv()
				try:
					data = json.loads(message)
				except json.decoder.JSONDecodeError:
					User.logger.warn("got malformed message from %s: %s" % (self.name, message))
					continue
				User.logger.info("got from %s: %s" % (self.name, message))
				await self.process_data(data)
		except websockets.exceptions.ConnectionClosed:
			pass
		finally:
			self.quit()

	async def process_data(self, data):
		data_type = data.get("type", None)
		if data_type is None:
			pass
			User.logger.warning("Got no data_type: %s" % data)
		# a few "hard coded" data handlers
		elif data_type == "chat_message":
			message = data.get("message", None)
			if message and self.lobby:
				if message[0] == "/":
					if message == "/list":
						await self.send_message("Online users: %s" % (", ".join(list( self.lobby.get_usernames() )),), "[Response]")
					elif message == "/help":
						await self.send_message("Known commands: /list: (list online players); /surrender: (surrender the match)", "[Response]")
					elif message == "/surrender":
						self.leave_match()
					else:
						await self.send_message("Unknown command: %s" % (message), "[Response]")
				else:
					asyncio.ensure_future( self.lobby.broadcast_message(message, author=self.name) )
		elif data_type == "enter_queue":
			self.leave_match()
			gamemode = data.get("gamemode", "standard")
			deck = data.get("deck", None)
			valid_deck = False
			try:
				valid_deck = deckbuilder.validate_deck(self, deck)
			except deckbuilder.NoSuchDeckError:
				valid_deck = False
			if valid_deck:
				if self.lobby.add_user_to_queue(self, gamemode, deck):
					await self.send_as_json({"type":"success_enter_queue", "gamemode":gamemode})
				else:
					await self.send_as_json({"type":"fail_enter_queue", "gamemode":gamemode})
			else:
				await self.send_as_json({"type":"invalid_deck", "deck": deck})
		elif data_type == "debug":
			if User.enable_debug_command:
				print(self)
		# in case I find a need to set data handlers from outside
		elif data_type in self._data_handlers:
			self._data_handlers[data_type](data)
		# matches will process many different types I guess so this prefix can signify those
		elif data_type[:2] == "M_":
			if self.match is not None:
				await self.match.handle_data(self.id, data)
		elif data_type[:2] == "P_":
			if self.match is not None:
				await self.match.handle_peer_data(self.id, data)
		# todo: move these elsewhere, long elif chains are not great
		elif data_type == "list_cards":
			await self.send_as_json({
				"type":"cards_loaded",
				"cards": deckbuilder.list_cards(self)
				})
		elif data_type == "list_decks":
			await self.send_as_json({
				"type":"decklist_loaded",
				"decks": deckbuilder.list_decks(self)
				})
		elif data_type == "load_deck":
			name_, cards_ = deckbuilder.load_deck(self, data.get("deckid"))
			await self.send_as_json({
				"type":"deck_loaded",
				"cards": cards_,
				"name": name_
				})
		# todo : save, rename, delete failed
		elif data_type == "save_deck":
			try:
				result = deckbuilder.save_deck(self, data.get("cards"), data.get("name"), data.get("deckid"))
				await self.send_as_json({
					"type":"deck_saved",
					"deckid": result[0],
					"name": result[1]
					})
			except ValueError as e:
				await self.send_as_json({
					"type":"deck_save_failed",
					"error": e.args[0]
					})
		elif data_type == "rename_deck":
			try:
				await self.send_as_json({
					"type":"deck_renamed",
					"name": deckbuilder.rename_deck(self, data.get("deckid"), data.get("name"))
					})
			except ValueError as e:
				await self.send_as_json({
					"type":"deck_rename_failed",
					"error": e.args[0]
					})
		elif data_type == "delete_deck":
			await self.send_as_json({
				"type":"deck_deleted",
				"result": deckbuilder.delete_deck(self, data.get("deckid"))
				})
		elif data_type == "load_all_cards":
			await self.send_as_json({
				"type":"all_cards_loaded",
				"cards": cards.cards
				})

	
	def set_data_handler(self, data_type, callback):
		self._data_handlers[data_type] = callback

	def add_lobby(self, lobby):
		self._lobby = lobby

	def rm_lobby(self, lobby):
		self._lobby = None

	def set_match(self, match):
		self._match = match
		asyncio.ensure_future( self.send_as_json({
			"type":"begin_match",
			"gamemode": match.gamemode,
			"players": match.player_ids
		}) )

	def leave_match(self):
		if self.match:
			self.match.leave(self)
			self._match = None
