from .tile import *
from .unit import Unit
from . import const
from . import effect_manager

import collections
import logging


class ConstBoard(const.BaseConst):
	ALLOWED = {
		"adj_coord_offsets",
		# "tiles",
		"units",
		"adjacent_coords",
		"is_coord_on_board",
		"summon_unit",
		"effect_manager",
	}

	def get_tile(self, coords):
		tile = self._wrappee.get_tile(coords)
		if tile is None:
			return None
		else:
			return tile.const

	def get_unit(self, uuid):
		unit = self._wrappee.get_unit(uuid)
		if unit is None:
			return None
		else:
			return unit.const

	def adjacent_tiles(self, coords):
		for tile in self._wrappee.adjacent_tiles(coords):
			yield tile.const

	def adjacent_units(self, coords):
		for unit in self._wrappee.adjacent_units(coords):
			yield unit.const

	@property
	def effect_manager(self):
		return self._wrappee._effect_manager.const

	@property
	def units(self):
		for unit in self._wrappee._units.values():
			yield unit
	



class Board():
	""" game board """
	adj_coord_offsets = (
		HexCoords(0,1,-1),HexCoords(1,0,-1),HexCoords(1,-1,0),
		HexCoords(0,-1,1),HexCoords(-1,0,1),HexCoords(-1,1,0)
		)

	logger = logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)

	def __init__(self, event_queue=None, gamemode="standard"):
		self.const = ConstBoard(self)
		self._event_queue = event_queue
		self._tiles = self.create_tiles()
		self._units = dict()
		self._event_listenening_units = collections.defaultdict(set)
		self._effect_manager = effect_manager.EffectManager(self._event_queue)


	def __repr__(self):
		return ", ".join([str(tile) for tile in self._tiles.values()])

	def __str__(self):
		return "<Board with %s tiles>" % len(self._tiles)


	@property
	def event_queue(self):
		return self._event_queue

	@property
	def effect_manager(self):
		return self._effect_manager

	# currently unused
	@property
	def tiles(self):
		for tile in self._tiles.values():
			yield tile


	def create_tiles(self):
		tiles = {}
		to_add = [HexCoords(0,0,0)]
		while len(to_add) > 0:
			coords = to_add.pop()
			if coords in tiles:
				continue
			tiles[coords] = Tile(coords)
			for adj_coord in self.adjacent_coords(coords):
				if adj_coord not in tiles:
					to_add.append(adj_coord)
		return tiles


	def get_tile(self, coords):
		return self._tiles.get(coords, None)

	def get_unit(self, uuid):
		return self._units.get(uuid, None)


	def adjacent_coords(self, coords):
		for adj in Board.adj_coord_offsets:
			adj_coords = HexCoords(coords[0]+adj[0], coords[1]+adj[1], coords[2]+adj[2])
			if self.is_coord_on_board(adj_coords):
				yield adj_coords

	def adjacent_tiles(self, coords):
		for coords in self.adjacent_coords(coords):
			yield self._tiles[coords]

	def adjacent_units(self, coords):
		for coords in self.adjacent_coords(coords):
			tile = self._tiles[coords]
			if tile.unit:
				yield tile.unit


	def is_coord_on_board(self, coords):
		if sum(coords) != 0:
			return False
		# hard coded board shape
		if abs(coords.x) > 4 or abs(coords.y) > 4 or abs(coords.z) > 3:
			return False
		return True


	def add_unit(self, unit):
		self._units[unit.uuid] = unit
		self.event_queue.put({"event":"unit_appeared", "unit":unit})
		for event_type in unit.event_listeners:
			self._event_listenening_units[event_type].add(unit)
		Board.logger.info("Added unit: %s %s" % (unit.uuid, unit.id))


	def remove_unit(self, unit):
		del self._units[unit.uuid]
		self.event_queue.put({"event":"unit_disappeared", "unit":unit})
		for event_type in unit.event_listeners:
			self._event_listenening_units[event_type].remove(unit)
		Board.logger.info("Removed unit")


	def handle_event(self, event, match):
		event_type = event["event"]

		if event_type == "really_change_turn":
			for unit in self._units.values():
				unit.new_turn(event.get("to"))
		for unit in self._event_listenening_units[event_type]:
			unit.handle_event(event_type, event, match)


	def summon_unit(self, card, coords, player_id):
		assert(self.is_coord_on_board(coords))
		assert(self._tiles[coords].unit is None)
		unit = Unit(card, self._tiles[coords], player_id, self)
		self.event_queue.put({"event":"summon_unit", "unit":unit})
		Board.logger.info("summoned unit")

