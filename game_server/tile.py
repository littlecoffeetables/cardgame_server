from . import const

import collections

HexCoords = collections.namedtuple("HexCoords", ["x", "y", "z"])


class ConstTile(const.BaseConst):
	ALLOWED = {
		"coords",
		"unit",
	}

	@property
	def unit(self):
		if self._wrappee._unit is None:
			return None
		else:
			return self._wrappee._unit.const
	


class Tile():
	""" a tile on a board """
	def __init__(self, hex_coords, unit=None):
		self.const = ConstTile(self)
		self._coords = hex_coords
		self.unit = unit

	@property
	def coords(self):
		return self._coords

	@property
	def is_occupied(self):
		return self.unit is not None

	def __repr__(self):
		if self.unit is None:
			return "<Tile(%s)>" % (self.coords,)
		else:
			return "<Tile(%s, unit=%s)>" % (self.coords, self.unit)
