from . import const
from . import database
from . import card_scripts

import json
import os
import sqlite3

def load_cards(json_dir_path):
	cards = {}
	with os.scandir(json_dir_path) as it:
		for entry in it:
			if entry.is_file() and entry.name.endswith(".json"):
				with open(entry.path, "r") as f:
					try:
						card_str = f.read()
						card = json.loads(card_str)
						versions = []
						cardid = card["id"]
						for i, version in enumerate(card["versions"]):
							versions.append(json.dumps(version))
							try:
								if i == 0:
									database.add_card(cardid)
								else:
									database.add_card_version(cardid, i+1)
							except sqlite3.IntegrityError:
								pass
						cards[cardid] = versions
					except json.JSONDecodeError as e:
						print("malformed json in", f.name)
						print(e)
	return cards

cards = load_cards("data/cards")

def get_card_str(cardid, version="latest"):
	versions = cards.get(cardid)
	if version == "latest" or version is None:
		return versions[-1]
	else:
		return versions[version-1]

def get_latest_version(cardid):
	versions = cards.get(cardid)
	return len(versions)

def get_script(cardid, script_name):
	module = getattr(card_scripts, cardid)
	script = getattr(module, script_name)
	return script


ALL_COLORS = ("red", "green", "blue")
ALL_GEM_TYPES = ("red", "green", "blue", "neutral")
COLORLESS = "neutral"

# Question: Would it be worth the hassle to check if a card has changed
#  and only send it the first time, or if it has changed?
# It could mean extra work client side if a card's data
#  may not always be available right away (when would it not be though?).
# Do cards ever actually change?
# Presumably yes if there are effects that eg. affect the cost of scrolls in your hand
#  but otherwise they should be mostly unchanging

class ConstCard(const.BaseConst):
	ALLOWED = {
		"uuid",
		"id",
		"version",
		"type",
		"subtypes",
		"unit_event_listeners",
		"special_scripts",
		"get_stat",
		"get_script",
	}

	def __str__(self):
		return str(self._wrappee)


class Card():
	""" class that instantiates a card
	card: dict containing all the data about the card
		loaded from json but may change during a match
	"""

	# uuid that will never be the same for any two cards,
	# (even across matches, for now),
	# as long as the server is running
	__current_uuid = 0

	def __init__(self, card_id, version=None, is_token=False):
		self.const = ConstCard(self)
		template = get_card_str(card_id, version)
		if template is None:
			print("ERROR template does not exist: %s, version %s" % (card_id, version))
			raise ValueError
		self.card = json.loads(template)
		self.card["id"] = card_id
		self.card["version"] = version
		if self.card.get("type") in ("creature","structure","commander"):
			self.card["is_unit"] = True
		self.card["uuid"] = Card.__current_uuid
		Card.__current_uuid += 1
		# every card is assumed to have stats
		# stats aren't statistics but like, power values
		if self.card.get("stats") is None:
			self.card["stats"] = {}
		self.card["is_token"] = is_token

	def __repr__(self):
		return json.dumps(self.card)

	def __str__(self):
		return json.dumps(self.card)

	@property
	def uuid(self):
		return self.card["uuid"]

	@property
	def id(self):
		return self.card["id"]

	@property
	def version(self):
		return self.card["version"]
	

	@property
	def type(self):
		return self.card["type"]

	@property
	def subtypes(self):
		for subtype in self.card.get("subtypes", []):
			yield subtype

	@property
	def name(self):
		return self.card["name"]

	@property
	def desc(self):
		return self.card.get("desc", "")

	@property
	def flavor(self):
		return self.card.get("flavor", "")

	@property
	def is_unit(self):
		return self.card.get("is_unit", False)

	@property
	def is_token(self):
		return self.card.get("is_token", False)
	
	@property
	def cost(self):
		return self.card["stats"].get("cost", 0)
	

	@property
	def unit_event_listeners(self):
		for item in self.card.get("unit_event_listeners", []):
			yield item

	@property
	def special_scripts(self):
		for item in self.card.get("special_scripts", []):
			yield item


	def get_script(self, script_name):
		return get_script(self.id, script_name)

	def get_stat(self, stat, default=None):
		value = self.card["stats"].get(stat, default)
		if stat == "gems":
			return dict(value)
		else:
			return value


	def set_stat(self, prop, value):
		self.card["stats"][prop] = value


