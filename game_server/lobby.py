from . import user
from . import match_controller

import asyncio
import random
import logging
import traceback


class Lobby(object):
	""" waiting room with chat and matchmaking
	users: dict of userid: User
	standard_match_queue: list of userid's queueing for a standard game
	"""

	logger = logging.getLogger(__name__)
	#logger.setLevel(logging.INFO)

	def __init__(self, print_broadcasts=True):
		# id, User
		self._users = dict()
		self._standard_match_queue = {}
		self._is_shutting_down = False
		self.print_broadcasts = print_broadcasts
		asyncio.ensure_future(self.make_matches())

	def __repr__(self):
		return "<Lobby users=%s standard_match_queue=%s>" % (
			["<User id=%s>"%(user.id,) for user in self._users.values()],
			self._standard_match_queue
			)
		
	def add_user(self, user):
		self._users[user.id] = user
		user.add_lobby(self)
		asyncio.ensure_future(self.broadcast_message("%s joined"%user.name))

	def rm_user(self, user):
		del self._users[user.id]
		user.rm_lobby(self)
		asyncio.ensure_future(self.broadcast_message("%s left"%user.name))

	async def broadcast_message(self, message, author=None):
		if self._is_shutting_down:
			return
		if self.print_broadcasts:
			print("broadcasting: %s"%message)
		if self._users:
			if author is None:
				author = "[Server]"
			message_json = user.make_message(message, author)
			await asyncio.wait([user.send_raw(message_json) for user in self._users.values()])

	async def shutdown(self):
		self._is_shutting_down = True
		for user in list(self._users.values()):
			# should this be an awaitable?
			user.quit()

	def has_username(self, username):
		return any(user.name == username for user in self._users.values())

	def has_userid(self, userid):
		return userid in self._users

	def get_usernames(self):
		for user in self._users.values():
			yield user.name

	def add_user_to_queue(self, user, gamemode, deckid):
		if gamemode == "standard":
			if user.id in self._standard_match_queue:
				return False
			else:
				self._standard_match_queue[user.id] = deckid
				return True
		else:
			return False

	def rm_user_from_queue(self, user, gamemode="all"):
		if gamemode == "standard" or gamemode == "all":
			try:
				del self._standard_match_queue[user.id]
			except KeyError:
				pass

	async def begin_match(self, userid1, userid2, gamemode):
		if userid1 == userid2:
			return
		Lobby.logger.info( "begin_match: %s vs %s" % (userid1, userid2) )
		user1 = self._users.get(userid1, None)
		user2 = self._users.get(userid2, None)
		decks = self._standard_match_queue[userid1], self._standard_match_queue[userid2]
		if user1 is not None and user2 is not None:
			try:
				match_controller.MatchController(user1, user2, gamemode, decks, self.debug_stop)
			except Exception as e:
				Lobby.logger.error(traceback.format_exc())
		await self.broadcast_message("Match started: %s vs %s" % (user1.name, user2.name))

	async def make_matches(self):
		matched = set()
		while not self._is_shutting_down:
			for gamemode in ("standard",):
				if gamemode == "standard":
					queue = list(self._standard_match_queue)
				else:
					raise NotImplementedError

				user_count = len(queue)
				for n in range(user_count//2):
					r1 = random.randrange(user_count)
					if r1 not in matched:
						r2 = random.randrange(user_count)
						if r2 not in matched and r1 is not r2:
							matched.add(r1)
							matched.add(r2)
							await self.begin_match(queue[r1], queue[r2], gamemode)
				for r in sorted(matched, reverse=True):
					userid = queue[r]
					del queue[r]
					del self._standard_match_queue[userid]
				matched.clear()

				if len(queue) < 100:
					await asyncio.sleep(1)

