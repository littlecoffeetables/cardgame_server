
# Provides an easy way to create an interface that only allows access to select attributes

# Simply subclass this and put allowed attribute names in the ALLOWED set

# Does not guarantee true constantness as there is no enforcement on what the wrapped methods may do


class BaseConst():
	ALLOWED = set()

	def __repr__(self):
		return "%s(_wrappee=%s)" % (type(self), self._wrappee)

	def __init__(self, wrappee):
		#self._wrappee = wrappee
		super().__setattr__("_wrappee", wrappee)

	def __getattr__(self, attr):
		if attr in self.ALLOWED:
			return getattr(self._wrappee, attr)
		elif hasattr(self._wrappee, attr):
			raise AttributeError("'%s' does not permit access to '%s'!" % (type(self), attr))
		else:
			raise AttributeError("(Const) '%s' has no attribute named '%s'" % (type(self._wrappee), attr))

	def __setattr__(self, attr, value):
		raise AttributeError("'%s' does not permit modifying '%s'!" % (type(self), attr))

