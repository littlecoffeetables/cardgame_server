from . import password
from . import config

import sqlite3
import traceback

conn = sqlite3.connect("database/hi.db")
c = conn.cursor()

c.executescript("""
	CREATE TABLE IF NOT EXISTS Player (
		pid integer primary key,
		displayname text,
		gold integer default 0,
		skill real default 0
	);

	CREATE TABLE IF NOT EXISTS UserAccount (
		player integer references Player(pid) primary key,
		username text,
		password text,
		email text,
		privileges integer default 0,
		UNIQUE (username)
	);

	CREATE TABLE IF NOT EXISTS Deck (
		did integer primary key,
		owner integer references Player(pid),
		name text,
		lastused text default null,
		lastmodified text,
		UNIQUE (owner, name)
	);

	CREATE TABLE IF NOT EXISTS Card (
		cardid text,
		version integer,
		PRIMARY KEY (cardid, version)
	);

	CREATE VIEW IF NOT EXISTS LatestCard (cardid, version)
	AS SELECT Card.cardid, max(Card.version)
	FROM Card
	GROUP BY Card.cardid;

	CREATE TABLE IF NOT EXISTS CardInstance (
		cid integer primary key,
		owner integer references Player(pid),
		cardid text,
		version integer,
		frozen integer default 0,
		unsellable integer default 0,
		FOREIGN KEY (cardid, version) REFERENCES Card (cardid, version)
	);

	CREATE TABLE IF NOT EXISTS CardInDeck (
		card integer references CardInstance(cid),
		deck integer references Deck(did),
		visualpos text default null,
		PRIMARY KEY (card, deck)
	);
	""")


def run_statement(statement):
	try:
		for line in c.execute(statement):
			print(line)
		conn.commit()
	except sqlite3.OperationalError:
		print(traceback.format_exc())


class UsernameTaken(Exception):
	pass

class InvalidLoginInfo(Exception):
	pass


def register_user(username, pw, email=""):
	c.execute("""SELECT username FROM UserAccount WHERE UserAccount.username = ?;""", (username,))
	if c.fetchone() is None:
		hash_str = password.create_hash(pw)
		c.execute("""
			INSERT INTO Player(displayname)
			VALUES (?);
			""", (username,)
			)
		userid = c.lastrowid
		c.execute("""
			INSERT INTO UserAccount (player, username, password, email)
			VALUES (?, ?, ?, ?);
			""", (userid, username, hash_str, email))
		if config.FREE_CARDS:
			grant_all_cards_to_player(userid, commit=False)
		conn.commit()
	else:
		raise UsernameTaken()


def login(username, pw):
	c.execute("""SELECT player, password FROM UserAccount WHERE UserAccount.username = ?;""", (username,))
	result = c.fetchone()
	if result is None:
		raise InvalidLoginInfo()
	else:
		pwh = result[1]
		if password.compare(pw, pwh):
			c.execute("""SELECT displayname FROM Player WHERE Player.pid = ?;""", (result[0],))
			# userid, displayname
			return result[0], c.fetchone()[0]
		else:
			raise InvalidLoginInfo()


def grant_all_cards_to_player(pid, commit=True):
	for i in range(config.MAX_COPIES_PER_CARD):
		c.execute("""
			INSERT INTO CardInstance (owner, cardid, version)
			SELECT ?, LatestCard.cardid, LatestCard.version
			FROM LatestCard
			;""", (pid,))
	if commit:
		conn.commit()


def grant_card_to_all_players(cardid, version=1, commit=True):
	for i in range(config.MAX_COPIES_PER_CARD):
		c.execute("""
			INSERT INTO CardInstance (owner, cardid, version)
			SELECT Player.pid, ?, ? FROM Player
			;""", (cardid, version))
	if commit:
		conn.commit()


def add_card(cardid):
	# cards must always start at version 1
	c.execute("""
		INSERT OR ABORT INTO Card (cardid, version)
		VALUES (?, 1)
		""", (cardid,))
	if config.FREE_CARDS:
		grant_card_to_all_players(cardid, commit=False)
	conn.commit()


def add_card_version(cardid, version):
	c.execute("""
		INSERT OR ABORT INTO Card (cardid, version)
		VALUES (?, ?);
		""", (cardid, version))
	c.execute("""
		UPDATE CardInstance
		SET CardInstance.version = ?
		WHERE CardInstance.cardid = ? and CardInstance.version < ? and CardInstance.frozen = 0;
		""", (version, cardid, version))
	conn.commit()


def load_deck_cards(deckid):
	c.execute("""
		SELECT CardInstance.cid, CardInstance.cardid, CardInstance.version
		FROM CardInstance, CardInDeck
		WHERE CardInstance.cid = CardInDeck.card and CardInDeck.deck = ?
		;
		""", (deckid,))
	return c.fetchall()

