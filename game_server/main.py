from .lobby import *
from . import user
from . import database
from . import config

import asyncio
import json
import logging
import websockets
import signal
import functools
import sys
import traceback

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# TODO make/use proper command parser
def handle_input(stop, lobby):
    line = sys.stdin.readline()[:-1]
    parts = line.split(" ", 1)
    if parts[0] == "help":
        if len(parts) > 1:
            if parts[1] == "help":
                print("help [command]: list known commands or show command usage")
            elif parts[1] == "say":
                print("say <message>: broadcast message")
            elif parts[1] == "run":
                print("run <code>: execute python code")
            elif parts[1] == "stop":
                print("stop: stop the server")
            elif parts[1] == "sql":
                print("sql <statement>: run sql statement")
        else:
            print("known commands: help, say, run, stop")
            print("for more info on a command: help <command>")
    elif parts[0] == "say":
        asyncio.ensure_future(lobby.broadcast_message(parts[1]))
    elif parts[0] == "run":
        exec(parts[1])
    elif parts[0] == "stop":
        terminated(stop)
    elif parts[0] == "sql":
        database.run_statement(parts[1])
    else:
        print("Unknown input")


async def handle_login(ws, lobby):
    quit = asyncio.Future()
    while True:
        message = await ws.recv()
        try:
            data = json.loads(message)
        except json.decoder.JSONDecodeError:
            logging.info("got invalid json: %s"%message)
            continue

        dtype = data.get('type')
        name = data.get('name', '')
        pw = data.get('password', '')
        # no database for now
        if user.is_valid_username(name):
            if dtype == "login":
                try:
                    user_id, displayname = database.login(name, pw)
                    if not lobby.has_userid(user_id):
                        user_ = user.User(ws, quit, displayname, user_id)
                        await ws.send(json.dumps({"type":"login_success", "motd":"Welcome!", "user_id":user_.id}))
                        lobby.add_user(user_)
                        break
                    else:
                        # maybe this should log out the old session instead of giving an error
                        logger.debug("used already logged in")
                        await ws.send(json.dumps({"type":"login_error","error":"You are logged in in another active session"}))
                except database.InvalidLoginInfo:
                    logger.debug("InvalidLoginInfo")
                    await ws.send(json.dumps({"type":"login_error","error":"Password or username incorrect"}))
            elif dtype == "register":
                try:
                    database.register_user(name, pw)
                    await ws.send(json.dumps({"type":"register_success", "username":name}))
                except database.UsernameTaken:
                    logger.debug("UsernameTaken")
                    await ws.send(json.dumps({"type":"login_error","error":"Username taken"}))
            else:
                logger.info("unsupported dtype on login: %s" % data)
        else:
            logging.info("unsupported event: %s"%data)
            await ws.send(json.dumps({"type":"login_error","error":"Illegal username"}))
    await quit

async def new_connection(lobby, ws, path):
    try:
        await handle_login(ws, lobby)
    except websockets.exceptions.ConnectionClosed:
        pass

async def create_server(stop, lobby, address, port):
    async with websockets.serve(functools.partial(new_connection, lobby), address, port):
        print("Hosting game server on", address, port)
        await stop
        await lobby.shutdown()

def terminated(stop):
    print("Terminating")
    stop.set_result(None)


def handle_exception(loop, context):
    msg = context.get("exception", context["message"])
    logging.error(f"Caught exception: {msg}")
    print(context)
    #logging.info("Shutting down...")
    #asyncio.create_task(shutdown(loop))


def main(address=None, port=None):
    lobby = Lobby()

    loop = asyncio.get_event_loop()

    loop.set_debug(True)

    stop = asyncio.Future()
    for sig in (signal.SIGTERM, signal.SIGINT):
        loop.add_signal_handler(sig, terminated, stop)
    lobby.debug_stop = stop

    loop.add_reader(sys.stdin, handle_input, stop, lobby)

    if address is None:
        address = config.DEFAULT_ADDRESS
    if port is None:
        port = config.DEFAULT_PORT

    loop.set_exception_handler(handle_exception)

    loop.run_until_complete(create_server(stop, lobby, address, port))


if __name__ == "__main__":
    main()
