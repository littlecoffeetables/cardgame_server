from .tile import *
from . import cards
from . import unit
from . import const
from . import database
from . import config

import random
import logging
import traceback


class InvalidInputError(Exception):
	pass


class ConstPlayer(const.BaseConst):
	ALLOWED = {
		"player_id",
		"opponent",
		"commander",
		"mana",
		"can_sacrifice",
		"total_gems",
		"has_gems",
		"send_soon",
	}

	@property
	def commander(self):
		return self._wrappee.commander.const


class Player():

	logger = logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)

	def __init__(self, player_id, deck, board=None, event_queue=None):
		self.const = ConstPlayer(self)
		self.disconnected = False
		self._to_send = []

		assert(player_id == 0 or player_id == 1)
		self._player_id = player_id
		self.event_queue = event_queue
		self.board = board
		self._opponent = None

		self.commander = None # Unit object?
		self._mana = 0
		self._gems = dict()
		self._can_sacrifice = True
		# uuid as key
		self._hand = dict()
		self.deckid = deck
		# undrawn cards
		self.library = []
		# spent cards
		self.graveyard = []


	def __repr__(self):
		return "<Player(), disconnected=%s>" % (self.disconnected,)

	@property
	def player_id(self):
		return self._player_id
	
	@property
	def mana(self):
		return self._mana

	@mana.setter
	def mana(self, value):
		self._mana = value
		self.player_resources_changed()
	
	@property
	def can_sacrifice(self):
		return self._can_sacrifice

	@property
	def gems(self):
		return dict(self._gems)

	@property
	def total_gems(self):
		return sum(self._gems.values())

	@property
	def opponent(self):
		return self._opponent.const


	def set_players(self, players):
		self._opponent = players[1 - self.player_id]


	def match_over(self, winner=None):
		result = "draw"
		if winner == self.player_id:
			result = "win"
		elif winner == self.opponent.player_id:
			result = "loss"
		self.send_soon({"type":"M_match_over", "result":result})


	#async def send(self, data):
	#	await self.user.send_as_json(data)


	def send_soon(self, data):
		self._to_send.append(data)

	#async def send_queued(self):
	#	Player.logger.info("Sending bundle for player %s" % self.user.name)
	#	await self.user.send_as_json({"type":"M_bundle", "bundle":self._to_send})
	#	self._to_send.clear()
	def retrieve_to_send(self):
		result = {
			"type": "M_bundle",
			"bundle": list(self._to_send)
		}
		self._to_send.clear()
		return result


	def setup_library(self):
		deck_cards = database.load_deck_cards(self.deckid)
		for cid, cardid, version in deck_cards:
			self.library.append(cards.Card(cardid, version))
		random.shuffle(self.library)


	def setup_match(self):
		self.setup_library()
		# todo: get commander from deck
		card = cards.Card("neutral_commander")
		dir_ = 1 if self.player_id == 0 else -1
		coords = HexCoords(dir_ * -3, dir_ * 3, 0)
		self.board.summon_unit(card, coords, self.player_id)
		if config.DEBUG_MODE:
			self._mana = 9
			self.add_gems("blue", 2)
			self.add_gems("red", 2)


	def new_turn(self):
		self.draw_cards(1)
		self.mana += self.total_gems
		self._can_sacrifice = True


	def get_gems(self, color, default=0):
		return self._gems.get(color, default)

	def add_gems(self, color, amount):
		self._gems[color] = self._gems.get(color, 0) + amount
		self.player_resources_changed()


	def convert_gems(self, to, from_="all", limit=None):
		converted = 0
		if from_ == "all":
			for color, amount in list(self._gems.items()):
				if color != to:
					#del self._gems[color]
					self._gems[color] = 0
					converted += amount
		else:
			available = self._gems.get(from_, 0)
			if available > 0:
				if limit is None:
					converted = available
				else:
					converted = min(limit, available)
				self._gems[from_] -= converted
		if converted > 0:
			self._gems[to] = self._gems.get(to, 0) + converted
			self.player_resources_changed()
		return converted


	# to play a card, a player must have at least the required amount of each colored gem
	# in addition, they should have at least the required amount of colorless gems
	# however colored gems that were not needed to satisfy a color requirement
	#  count towards the colorless requirement
	def has_gems(self, gems):
		surplus = 0
		for color in cards.ALL_COLORS:
			req_amount = gems.get(color, 0)
			has_amount = self._gems.get(color, 0)
			diff = has_amount - req_amount
			if diff < 0:
				return False
			else:
				surplus += diff
		req_neutral = gems.get(cards.COLORLESS, 0)
		has_neutral = self._gems.get(cards.COLORLESS, 0)
		if has_neutral + surplus < req_neutral:
			return False
		return True


	def player_resources_changed(self):
		obj = {"type":"M_resource_update", "player":self.player_id, "mana":self.mana, "gems":self._gems}
		self.send_soon(obj)
		self.opponent.send_soon(obj)
		self.event_queue.put({"event": "player_resources_changed"})


	def draw_cards(self, count):
		actually_drawn = []
		for i in range(count):
			if len(self.library) > 0:
				card = self.library.pop()
				actually_drawn.append(card)
				self.add_card_to_hand(card, send=False)
		# send in batch to save on expensive websocket io calls
		if len(actually_drawn) > 0:
			self.send_soon({
				"type":"M_draw_card", "cards":[str(card) for card in actually_drawn], "player": self.player_id})
			self.opponent.send_soon({
				"type":"M_draw_card", "count":len(actually_drawn), "player": self.player_id})


	def add_card_to_hand(self, card, send=True):
		self._hand[card.uuid] = card
		if send:
			self.send_soon({
				"type":"M_draw_card", "card":str(card), "player": self.player_id})
			self.opponent.send_soon({
				"type":"M_draw_card", "count":1, "player": self.player_id})


	def rm_card_from_hand(self, card, sacced_for=None):
		uuid = card.uuid
		obj = {"type":"M_rm_card", "player": self.player_id}
		if sacced_for is not None:
			obj["sacced_for"] = sacced_for

		opponent_obj = dict(obj)
		opponent_obj["count"] = 1
		self.opponent.send_soon(opponent_obj)
		obj["uuid"] = uuid
		self.send_soon(obj)

		self.graveyard.append(card)
		del self._hand[uuid]


	def play_unit(self, card, coords):
		self.board.summon_unit(card, coords, self.player_id)
		self.rm_card_from_hand(card)

	def play_spell(self, card, coords, match):
		spell = card.get_script("spell")
		target_tiles = [self.board.get_tile(coords)]
		spell(card, match, self.player_id, target_tiles)
		self.rm_card_from_hand(card)


	def try_play_card(self, data, match):
		Player.logger.debug("trying to play card: %s" % data)
		uuid = data.get("uuid")
		if uuid is None:
			raise InvalidInputError("no uuid given")

		success = False

		card = self._hand.get(uuid)

		if card is None:
			self.send_soon({
				"type": "M_rm_card",
				"uuid":uuid
				})
			raise InvalidInputError("no card with uuid %s in hand: %s" % (uuid, self._hand))

		if card.cost > self.mana:
			raise InvalidInputError("insufficient mana")
		gems = card.get_stat("gems")
		if gems is not None and not self.has_gems(gems):
			raise InvalidInputError("insufficient gems")

		# TODO: support multiple tiles
		coords = data.get("coords")
		try:
			coords = HexCoords(*coords)
		except Exception:
			raise InvalidInputError("invalid coords")

		if card.is_unit:
			if coords is None or not self.board.is_coord_on_board(coords):
				raise InvalidInputError("coords not on board")
			if self.board.get_tile(coords).is_occupied:
				raise InvalidInputError("tile is occupied")
			if not any(unit.owner == self.player_id for unit in self.board.adjacent_units(coords)):
				raise InvalidInputError("not adjacent to friendly")
			self.play_unit(card, coords)
			success = True
		else:
			if coords is None or not self.board.is_coord_on_board(coords):
				raise InvalidInputError("coords not on board")
			try:
				self.play_spell(card, coords, match)
				success = True
			except Exception:
				Player.logger.debug(traceback.format_exc())

		if success:
			self.mana -= card.cost

		Player.logger.info("done playing card")
		return success


	def try_sacrifice(self, data):
		card_uuid = data.get("uuid")
		sac_for = data.get("sac_for")
		card = self._hand.get(card_uuid)
		if self.can_sacrifice and card is not None:
			if sac_for == "cards":
				self.draw_cards(2)
			elif sac_for in cards.ALL_GEM_TYPES:
				self.add_gems(sac_for, 1)
				self.mana += 1
			else:
				return False
			self.rm_card_from_hand(card, sacced_for=sac_for)
			self.event_queue.put({"event":"sacced_card", "sac_for":sac_for, "card":card, "player":self.player_id})
			self._can_sacrifice = False
			return True
		return False


	def try_move_unit(self, data):
		Player.logger.info("trying to move unit: %s" % data)
		card_uuid = data.get("uuid")
		unit = self.board.get_unit(card_uuid)
		if unit is None or unit.owner != self.player_id:
			return False

		if not unit.can_move():
			return False

		coords = data.get("to")
		try:
			coords = HexCoords(*coords)
		except Exception: #except Exception is bad
			return False

		if coords not in self.board.adjacent_coords(unit.tile.coords):
			return False

		tile = self.board.get_tile(coords)
		if tile is None or tile.unit is not None:
			return False

		self.event_queue.put({"event":"unit_move", "unit":unit, "to_tile":tile})
		return True


	def try_attack_with_unit(self, data):
		unit_uuid = data.get("uuid")
		target_uuid = data.get("target")
		if unit_uuid is None or target_uuid is None:
			return False

		unit = self.board.get_unit(unit_uuid)
		target = self.board.get_unit(target_uuid)
		if unit is None or target is None:
			return False

		if not unit.can_attack():
			return False

		self.event_queue.put({"event":"unit_attack", "unit":unit, "target":target})
		return True


