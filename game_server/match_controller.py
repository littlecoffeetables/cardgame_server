from . import match
from . import player
from . import board
from . import event_queue

import asyncio
import logging
import traceback


class MatchController():

	legit_data_types = {
		"M_play_card",
		"M_endturn",
		"M_sac_card",
		"M_unit_move",
		"M_unit_attack"
	}
	legit_anytime_data_types = {
	}
	legit_peer_data_types = {
		"P_hover",
		"P_endhover"
	}

	interesting_event_types = {
		"unit_appeared",
		"unit_disappeared",
		"gameover",
		"end_turn",
		"really_change_turn",
		"unit_move",
		"unit_attack",
		"unit_moved",
		"unit_stat_changed",
		"unit_take_damage",
		"unit_die",
		"effect_added",
		"effect_gone",
		"runscript",
	}
	uninteresting_event_types = {
		"summon_unit",
		"sacced_card",
		"unit_died",
		"unit_attacked",
		"unit_counter_attacked",
	}

	logger = logging.getLogger(__name__)
	logger.setLevel(logging.INFO)


	def __init__(self, user1, user2, gamemode="standard", decks=None, stop=None):
		self.event_queue = event_queue.EventQueue()

		self._users = [user1, user2]
		self._player_ids = {user1.id: 0, user2.id: 1}

		if decks is None:
			raise TypeError("decks must be given")

		self.board = board.Board(gamemode=gamemode, event_queue=self.event_queue.const)

		self.players = (
			player.Player(0, decks[0], board=self.board, event_queue=self.event_queue.const),
			player.Player(1, decks[1], board=self.board, event_queue=self.event_queue.const)
		)
		for plr in self.players:
			plr.set_players(self.players)

		self.match = match.Match(self.players, self.board, self.event_queue.const, gamemode)

		for user in self._users:
			user.set_match(self)

		self.stop = stop
		#self._task = asyncio.ensure_future(self.run())
		self._task = asyncio.create_task(self.run())


	def __repr__(self):
		return "<MatchController(<User name=%s id=%s>, <User name=%s id=%s>, Match=%s)>" % (
			self._users[0].name, self._users[0].id,
			self._users[1].name, self._users[1].id,
			self.match
			)

	@property
	def player_ids(self):
		return self._player_ids

	@property
	def gamemode(self):
		return self.match.gamemode


	async def run(self):
		try:
			MatchController.logger.debug("setting up match")
			self.match.setup_match()
			await self.send_all_queued()
			while not self.match.is_over:
				nxt = await self.event_queue.get()
				self.process_event(nxt)
				await self.send_all_queued()
		except asyncio.CancelledError:
			MatchController.logger.warning("MatchController.run() cancelled")
			raise asyncio.CancelledError
		except Exception as e:
			MatchController.logger.error(traceback.format_exc())
			self.match_over()
			self.stop.set_result(None)
			raise asyncio.CancelledError


	async def send_all_queued(self):
		MatchController.logger.debug("send_all_queued")
		tasks = []
		for i, user in enumerate(self._users):
			tasks.append(user.send_as_json(self.players[i].retrieve_to_send()))
		await asyncio.gather(*tasks)


	def leave(self, user):
		# TODO add small mercy time for reconnections before ending a match due to a dc
		self.match.leave(self._player_ids[user.id])
		self.match_over()

	def match_over(self):
		if self.match.is_over:
			return
		self.match.match_over()
		asyncio.ensure_future(self.cleanup())

	async def cleanup(self):
		MatchController.logger.info("cleanup")
		await self.send_all_queued()
		self._task.cancel()


	def get_mutable(self, const_obj):
		# this is a dirty hack until I consider how best to accomplish the same properly
		# options include at least:
		#  - keep a cache of all tiles, units and cards in the match object
		#  - find them via the board
		if const_obj is None:
			return None
		else:
			try:
				return const_obj._wrappee
			except AttributeError:
				return const_obj


	# For data that should get forwarded as-is to the opponent and spectators
	# not anything the server needs to act upon
	# so basically just for aesthetic effects
	# to make the player feel like their opponent really is a human
	async def handle_peer_data(self, userid, data):
		player_id = self._player_ids[userid];
		data_type = data["type"]
		if data_type in MatchController.legit_peer_data_types:
			await self._users[1 - player_id].send_as_json(data)


	async def handle_data(self, userid, data):
		player_id = self._player_ids[userid];
		data_type = data["type"]
		if data_type in MatchController.legit_data_types:
			if player_id == self.match.current_player_id:
				data["player_id"] = player_id
				self.event_queue.put_input(data)
			else:
				await self._users[player_id].send_as_json({"type":"M_not_your_turn"})
		elif data_type in MatchController.legit_anytime_data_types:
			raise NotImplementedError
		else:
			await self._users[player_id].send_as_json({"type":"M_bad_type", "data_type":data_type})


	def process_event(self, event):
		data_type = event.get("type")
		MatchController.logger.debug("process_event: %s" % event)
		try:
			if data_type is not None:
				try:
					self.process_input(event)
				except Exception as e:
					print(traceback.format_exc())
			elif event.get("event") is not None:
				self.process_noninput(event)
			else:
				MatchController.logger.error("bad event:", event)
				raise ValueError
		except AssertionError as e:
			MatchController.logger.error("AssertionError during processing of event: %s" % event)
			print(traceback.format_exc())


	def process_noninput(self, event):
		event_type = event["event"]
		MatchController.logger.info("process_noninput: %s" % event_type)
		if event_type in MatchController.interesting_event_types:
			handler_name = "%s_handler" % event_type
			if hasattr(self, handler_name):
				handler = getattr(self, handler_name)
				handler(event)
			else:
				MatchController.logger.warning("no handler for interesting event type: %s" % event_type)
		elif event_type in MatchController.uninteresting_event_types:
			pass
		else:
			MatchController.logger.warning("unrecognized event type: %s" % event_type)
		self.board.handle_event(event, self.match)


	def unit_appeared_handler(self, event):
		unit = event.get("unit")
		assert(unit is not None)
		if unit.is_commander:
			self.players[unit.owner].commander = unit
		for player in self.players:
			player.send_soon({"type":"M_event_unit_appeared", "unit":str(unit)})

	def unit_disappeared_handler(self, event):
		unit = event.get("unit")
		assert(unit is not None)
		for player in self.players:
			player.send_soon({"type":"M_event_unit_disappeared", "unit":unit.uuid})
		if unit.is_commander:
			MatchController.logger.info("commander down")
			self.event_queue.put_special({"event":"gameover"}, 1)

	def gameover_handler(self, event):
		if self.match.winner is None:
			winner = 2
			if self.players[0].commander.is_destroyed:
				winner -= 1
			if self.players[1].commander.is_destroyed:
				winner -= 2
			self.match.winner = winner
		self.match_over()

	def end_turn_handler(self, event):
		from_player_id = event.get("from_player_id")
		if from_player_id == self.match.current_player_id:
			self.event_queue.put_special({"event":"really_change_turn"}, 20)

	def really_change_turn_handler(self, event):
		self.match.change_turn()
		event["to"] = self.match.current_player_id

	def unit_move_handler(self, event):
		unit = self.get_mutable(event.get("unit"))
		assert(unit is not None)
		to_tile = self.get_mutable(event.get("to_tile"))
		assert(to_tile is not None)
		unit.move(to_tile)

	def unit_attack_handler(self, event):
		unit = self.get_mutable(event.get("unit"))
		assert(unit is not None)
		target = self.get_mutable(event.get("target"))
		assert(target is not None)
		unit.attack(target)

	def unit_moved_handler(self, event):
		unit = event.get("unit")
		from_tile = event.get("from_tile")
		to_tile = event.get("to_tile")
		assert(None not in (unit, from_tile, to_tile))
		for player in self.players:
			player.send_soon({"type":"M_event_unit_moved", "uuid":unit.uuid,
							"from":from_tile.coords, "to":to_tile.coords})

	def unit_stat_changed_handler(self, event):
		unit = event.get("unit")
		stat = event.get("stat")
		new_value = event.get("new_value")
		assert(None not in (unit, stat))
		# TODO: consider buffering these instead of sending each one individually
		for player in self.players:
			player.send_soon({"type":"M_event_stats_changed", "unit":unit.uuid,
							"stat": stat, "new_value":new_value})

	def unit_take_damage_handler(self, event):
		unit = event.get("unit")
		amount = event.get("amount")
		dmg_type = event.get("dmg_type")
		assert(None not in (unit, amount, dmg_type))
		unit.take_damage(amount, dmg_type)

	def unit_die_handler(self, event):
		unit = event.get("unit")
		assert(unit is not None)
		unit.try_die()

	def effect_added_handler(self, event):
		effect = event.get("effect")
		assert(effect is not None)
		target = effect.get("target_uuid")
		if target is not None:
			self.match.board.get_unit(target).add_effect(effect)

	def effect_gone_handler(self, event):
		effect = event.get("effect")
		assert(effect is not None)
		target = effect.get("target_uuid")
		if target is not None:
			self.match.board.get_unit(target).rm_effect(effect)

	def runscript_handler(self, event):
		callback = event.get("callback")
		assert(callback is not None)
		unit = event.get("unit")
		if unit:
			callback(unit, self.match, event)
		else:
			callback(self.match, event)



	def process_input(self, data):
		data_type = data["type"]
		MatchController.logger.info("process_input: %s" % data_type)
		player_id = data.get("player_id")
		assert(player_id in (0, 1))
		plr = self.players[player_id]

		valid = None

		try:
			if data_type == "M_play_card":
				valid = plr.try_play_card(data, self.match)
			elif data_type == "M_endturn":
				valid = True
				if self.match.current_player_id == player_id:
					self.event_queue.put({"event":"end_turn", "from_player_id":player_id})
			elif data_type == "M_sac_card":
				valid = plr.try_sacrifice(data)
			elif data_type == "M_unit_move":
				valid = plr.try_move_unit(data)
			elif data_type == "M_unit_attack":
				valid = plr.try_attack_with_unit(data)
			else:
				MatchController.logger.error("crap, not implemented", data)
				raise NotImplementedError
		except player.InvalidInputError as e:
			valid = False
			if len(e.args) > 0:
				MatchController.logger.debug(e.args[0])

		if valid is None:
			MatchController.logger.warning("uh oh, valid is None", data)
		if not valid:
			plr.send_soon({"type":"M_invalid","data":data})
		else:
			MatchController.logger.info("successfully handled input: %s" % data)
