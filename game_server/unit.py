from . import const

import json
import logging


class ConstUnit(const.BaseConst):
	ALLOWED = {
		"card",
		"tile",
		"owner",
		"board",
		"coords",
		"uuid",
		"id",
		"type",
		"subtypes",
		"is_commander",
		"is_destroyed",
		"get_base_stat",
		"get_effective_stat",
		"get_max_stat",
		"can_move",
		"can_attack",
		"can_counter_attack",
	}


class Unit():
	""" a unit on a tile on the board """

	logger = logging.getLogger(__name__)
	logger.setLevel(logging.INFO)

	def __init__(self, card, tile, owner, board):
		self.const = ConstUnit(self)
		self._card = card.const
		self._tile = tile
		self._tile.unit = self
		# player id
		self.owner = owner
		self._board = board
		self._current_stats = {}
		self.setup_stats()
		self._is_destroyed = False
		self._special_scripts = {}
		self._event_listeners = {}
		self.init_event_listeners()
		self._board.add_unit(self)

		
	def __repr__(self):
		return "<Unit(%s, %s, %s)>" % (self.card, self._tile, self.owner)

	def __str__(self):
		return json.dumps({"card":str(self.card), "coords":self.coords, "owner":self.owner, "unit_stats":self._current_stats})


	@property
	def coords(self):
		return self._tile.coords

	@property
	def board(self):
		return self._board.const

	@property
	def tile(self):
		return self._tile.const

	@property
	def card(self):
		return self._card

	@property
	def uuid(self):
		return self.card.uuid

	@property
	def id(self):
		return self.card.id

	@property
	def type(self):
		return self.card.type

	@property
	def subtypes(self):
		return self.card.subtypes


	@property
	def is_commander(self):
		return self.type == "commander"

	@property
	def is_structure(self):
		return self.type == "structure"
	

	@property
	def is_destroyed(self):
		return self._is_destroyed

	@property
	def event_listeners(self):
		return self._event_listeners.keys()
		


	def cleanup(self):
		self._tile.unit = None
		self._board.remove_unit(self)
		self._is_destroyed = True


	def get_script(self, script_name):
		return self._card.get_script(script_name)

	def init_event_listeners(self):
		event_listeners = self.card.unit_event_listeners
		if event_listeners is not None:
			for event_type in event_listeners:
				Unit.logger.info("adding event listener: %s" % event_type)
				self._event_listeners[event_type] = self.get_script(event_type)
		special_scripts = self.card.special_scripts
		if special_scripts is not None:
			for script in special_scripts:
				Unit.logger.info("adding special script: %s" % script)
				self._special_scripts[script] = self.get_script(script)
		Unit.logger.info("done initializing scripts")


	def handle_event(self, event_type, event, match):
		Unit.logger.info("handling event: %s" % event_type)
		if event_type in self._event_listeners:
			self._event_listeners[event_type](self, match.const, event)
		Unit.logger.info("done handling event: %s" % event_type)


	def setup_stats(self):
		self._current_stats["max_speed"] = self.get_base_stat("speed")
		self._current_stats["max_hp"] = self.get_base_stat("hp")
		self._current_stats["atk"] = self.get_base_stat("atk")
		self._current_stats["speed"] = 0
		self._current_stats["hp"] = self._current_stats["max_hp"]

	# readonly and can (almost) never change
	# should this infer the unprefixed variant?
	def get_base_stat(self, stat):
		assert(stat in ("hp", "atk", "speed", "max_hp", "max_speed"))
		if stat in ("max_hp", "max_speed"):
			return self.card.get_stat(stat[4:])
		else:
			return self.card.get_stat(stat)

	# todo: make this private
	def get_base_current_stat(self, stat):
		assert(stat in ("hp", "atk", "speed", "max_hp", "max_speed"))
		current = self._current_stats.get(stat)
		if current is not None:
			return current
		else:
			return self.get_base_stat(stat)

	# effective values are readonly
	def get_effective_stat(self, stat):
		assert(stat in ("hp", "atk", "speed", "max_hp", "max_speed"))
		effective = self.get_base_current_stat(stat)
		# are all modifiers in effect form?
		for effect in self.board.effect_manager.get_effects_by_target(self.uuid):
			modifiers = effect.get("modifiers")
			if modifiers:
				effective += modifiers.get(stat, 0)
		return effective

	def get_max_stat(self, stat):
		return self.get_effective_stat("max_%s" % stat)


	def add_effect(self, effect):
		modifiers = effect.get("modifiers")
		if modifiers:
			for modifier in modifiers:
				self.stat_changed(modifier)

	def rm_effect(self, effect):
		modifiers = effect.get("modifiers")
		if modifiers:
			for modifier in modifiers:
				self.stat_changed(modifier)

	def stat_changed(self, stat):
		new_value = self.get_effective_stat(stat)
		self._board.event_queue.put({
			"event": "unit_stat_changed",
			"stat": stat,
			"unit": self,
			"new_value": new_value
			})
		if stat == "hp" and new_value <= 0:
			self._board.event_queue.put({"event":"unit_die", "unit":self})

	def set_stat(self, stat, value):
		self._current_stats[stat] = value
		self.stat_changed(stat)

	def modify_stat(self, stat, amount):
		self.set_stat(stat, amount + self.get_base_current_stat(stat))


	def heal(self, amount):
		old_hp = self.get_base_current_stat("hp")
		new_hp = min(self.get_max_stat("hp"), amount + old_hp)
		self.set_stat("hp", new_hp)
		diff = new_hp - old_hp
		self._board.event_queue.put({"event":"unit_healed", "unit":self, "amount":diff})


	def take_damage(self, amount, dmg_type="magic"):
		self.modify_stat("hp", -amount)


	def try_die(self):
		if self.get_effective_stat("hp") <= 0:
			self._board.event_queue.put({"event":"unit_died", "unit":self})
			self.cleanup()


	def new_turn(self, player_id):
		if player_id == self.owner:
			if self.is_structure:
				if self.get_effective_stat("speed") < self.get_max_stat("speed"):
					self.modify_stat("speed", 1)
			else:
				self.set_stat("speed", self.get_max_stat("speed"))


	def can_move(self):
		if not self.get_effective_stat("speed") > 0:
			return False
		if "can_move" in self._special_scripts:
			return self._special_scripts["can_move"](self, self.board)
		else:
			return not self.is_structure

	def can_attack(self):
		if not self.get_effective_stat("speed") > 0:
			return False
		if "can_attack" in self._special_scripts:
			return self._special_scripts["can_attack"](self, self.board)
		else:
			return not self.is_structure

	def can_counter_attack(self):
		return True


	def counter_attack(self, attacker):
		if self.can_counter_attack():
			damage = (self.get_effective_stat("atk") + 1) // 2
			self._board.event_queue.put({"event":"unit_counter_attacked", "unit":self, "target":attacker, "damage":damage})
			self._board.event_queue.put({"event":"unit_take_damage", "unit":attacker, "dmg_type":"physical", "amount":damage})


	def move(self, to_tile):
		old_tile = self._tile
		self._tile.unit = None
		self._tile = to_tile
		self._tile.unit = self
		self.modify_stat("speed", -1)
		# TODO: set other things such as once-per-turn move
		self._board.event_queue.put({"event":"unit_moved", "unit":self, "from_tile":old_tile, "to_tile":self._tile})


	def attack(self, target):
		damage = self.get_effective_stat("atk")
		# TODO: target-specific damage modifiers
		self.modify_stat("speed", -1)
		self._board.event_queue.put({"event":"unit_attacked", "unit":self, "target":target, "damage":damage})
		self._board.event_queue.put({"event":"unit_take_damage", "unit":target, "dmg_type":"physical", "amount":damage})
		# maybe move this to the default unit_attacked handler of the target?
		target.counter_attack(self)
