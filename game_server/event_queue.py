from . import const

import queue
import asyncio
import logging


class ConstEventQueue(const.BaseConst):
	# not purely const as it is meant to serve as the route on which
	#  information from the game flows back to the match object
	ALLOWED = {
		"put",
	}


class EventQueue():
	"""
	A wrapper around a standard queue
	Makes sure events are handled one by one in the right order
	One day this might also be useful for recording replays,
		keeping battle logs, etc
	"""
	logger = logging.getLogger(__name__)
	#logger.setLevel(logging.INFO)

	def __init__(self):
		self.const = ConstEventQueue(self)
		self._ev_q = queue.Queue()
		self._io_q = queue.Queue()
		self._special_q = queue.PriorityQueue()
		self._can_get_event = asyncio.Event()

	def put_input(self, data):
		EventQueue.logger.info("scheduling input event: %s" % data)
		self._io_q.put(data)
		self._can_get_event.set()

	def put(self, data):
		EventQueue.logger.info("scheduling normal event: %s" % data)
		if data.get("type") is not None:
			EventQueue.warning("scheduling non-input event with a type: %s" % data)
		self._ev_q.put(data)
		self._can_get_event.set()

	def put_special(self, data, priority):
		EventQueue.logger.info("scheduling special event with priority %s: %s" % (priority, data))
		if data.get("type") is not None:
			EventQueue.warning("scheduling special event with a type: %s" % data)
		# worth noting that priority queues actually get the lowest priority item first, I'm pretty sure
		self._special_q.put((priority, data))
		self._can_get_event.set()

	def empty(self):
		return self._ev_q.empty() and self._special_q.empty() and self._io_q.empty()

	def get_blocking(self):
		if self._ev_q.empty():
			if self._special_q.empty():
				return self._io_q.get()
			else:
				return self._special_q.get()[1]
		else:
			return self._ev_q.get()

	async def get(self):
		await self._can_get_event.wait()
		result = self.get_blocking()
		if self.empty():
			self._can_get_event.clear()
		return result
