from . import const

import logging


class ConstMatch(const.BaseConst):
	ALLOWED = {
		"gamemode",
		"event_queue",
		"players",
		"board",
		"current_player_id",
		"is_over",
		"runscript",
	}

	@property
	def board(self):
		return self._wrappee._board.const



class Match():

	logger = logging.getLogger(__name__)
	logger.setLevel(logging.DEBUG)


	def __init__(self, players, board, event_queue, gamemode="standard"):
		self.const = ConstMatch(self)
		self.gamemode = gamemode

		self.event_queue = event_queue

		self._players = tuple(players)
		#self.players = tuple( plr.const for plr in self._players )

		self._board = board

		self.current_player_id = 0
		self.is_over = False
		self.winner = None


	def __repr__(self):
		return "<Match(gamemode=%s, board=%s)>" % (
			self.gamemode, self.board
			)

	@property
	def players(self):
		# does this break anything?
		return self._players[0], self._players[1]

	@property
	def board(self):
		return self._board


	def setup_match(self):
		for player in self._players:
			player.setup_match()
		self._players[0].draw_cards(5)
		self._players[1].draw_cards(5)
		self.both_players_resources_update()


	def leave(self, player_id):
		self._players[player_id].disconnected = True
		# TODO add small mercy time for reconnections before ending a match due to a dc
		if self.winner is None:
			winner = 2
			if self._players[0].disconnected:
				winner -= 1
			if self._players[1].disconnected:
				winner -= 2
			self.winner = winner


	def match_over(self):
		if self.is_over:
			return
		if self.winner not in (0, 1):
			self.winner = None
		print("match_over", self.winner)
		for player in self._players:
			player.match_over(self.winner)
		self.is_over = True


	def change_turn(self):
		self.current_player_id = 1 - self.current_player_id
		for player in self._players:
			player.send_soon({"type":"M_change_turn", "to":self.current_player_id})
		self._players[self.current_player_id].new_turn()


	# equivalent to `player.player_resources_changed() for player in self.players`
	def both_players_resources_update(self):
		obj = {
				"type": "M_resource_update",
				"player": "all",
				"mana": [self.players[0].mana, self.players[1].mana],
				"gems": [self._players[0].gems, self._players[1].gems]
			}
		for player in self._players:
			player.send_soon(obj)

	def runscript(self, unit, callback):
		self.event_queue.put({
				"event": "runscript",
				"callback": callback,
				"unit": unit,
			})


