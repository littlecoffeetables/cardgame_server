import os
import hashlib

SALT_LEN = 16
# changing any of these will invalidate any old hashes in the database
ITER_COUNT = 100000
ALGO = 'sha256'
SEP = ';'


def create_hash(pw_str):
	salt = os.urandom(SALT_LEN)
	pwb = bytes(pw_str, 'utf8')
	dk = hashlib.pbkdf2_hmac(ALGO, pwb, salt, ITER_COUNT)
	result = "%s%s%s" % (salt.hex(), SEP, dk.hex())
	return result


def compare(pw_str, hash_str):
	salts, correct_dkh = hash_str.split(SEP)
	pwb = bytes(pw_str, 'utf8')
	salt = bytes.fromhex(salts)
	dk = hashlib.pbkdf2_hmac(ALGO, pwb, salt, ITER_COUNT)
	return dk.hex() == correct_dkh

